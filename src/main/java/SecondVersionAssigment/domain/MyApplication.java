package SecondVersionAssigment.domain;


import org.w3c.dom.ls.LSOutput;

import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.SQLOutput;
import java.util.*;

public class MyApplication {

    private Scanner sc = new Scanner(System.in);
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    File file = new File("C:\\Users\\didef\\IdeaProjects\\2VersionAssigment\\src\\main\\java\\SecondVersionAssigment\\domain\\db.txt");
    File Messages = new File("C:\\Users\\didef\\IdeaProjects\\2VersionAssigment\\src\\main\\java\\SecondVersionAssigment\\domain\\Messages");

    private User signedUser;
    private ArrayList<User> listOfUsers = new ArrayList<User>();
    private User admin = new Admin();

    private void menu() {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in");
                System.out.println("1. Authentication");
                System.out.println("2. Back");
                int choice = sc.nextInt();
                if (choice == 1) authentication();
                else break;
            }
            else if(((Admin)admin).isAdmin()){
                System.out.println("Admin");
                System.out.println("1. All signed users");
                System.out.println("2. Users account status");
                System.out.println("3. Users messages");
                System.out.println("4. Profile");
                int w = sc.nextInt();
                if(w == 1){
                    showUsers();
                }
                else if(w == 2) {
                    System.out.println("1.Banned users");
                    System.out.println("2.Ban User");
                    System.out.println("3.UnBan User");
                    System.out.println("4.Back");
                    int a = sc.nextInt();
                    if (a == 1) {
                        int k = 0;
                        for (int i = 0; i < listOfUsers.size(); i++) {
                            if (listOfUsers.get(i).getName().contains("BANNED") && listOfUsers.get(i).getSurname().contains("BANNED")) {
                                System.out.println(listOfUsers.get(i));
                                k++;
                            }
                        }
                        if(k==0){
                            System.out.println("There are no banned users");
                        }
                    }
                    else if (a == 2) {
                        System.out.println("Ban user, write User's username");
                        String banedUs = null;
                        try {
                            banedUs = reader.readLine();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        for (int i = 0; i < listOfUsers.size(); i++) {
                            if (banedUs.equals(listOfUsers.get(i).getUsername())) {
                                String x = listOfUsers.get(i).getName();
                                String y = listOfUsers.get(i).getSurname();
                                listOfUsers.get(i).setName(x + "BANNED");
                                listOfUsers.get(i).setSurname(y + "BANNED");
                            }
                        }
                        System.out.println("User is BANNED");
                    }
                    else if(a == 3){
                        System.out.println("UnBan user, write User's username");
                        String UnBanedUs = null;
                        try {
                            UnBanedUs = reader.readLine();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        for (int i = 0; i < listOfUsers.size(); i++) {
                            if (UnBanedUs.equals(listOfUsers.get(i).getUsername())) {
                                String x = listOfUsers.get(i).getName();
                                x = x.replaceAll("BANNED", "");
                                String y = listOfUsers.get(i).getSurname();
                                y = y.replaceAll("BANNED", "");
                                listOfUsers.get(i).setName(x);
                                listOfUsers.get(i).setSurname(y);
                            }
                        }
                        System.out.println("User is unBanned");
                    }
                }
                else if(w == 3) {
                    int k = 0;
                    for (int i = 0; i < listOfUsers.size(); i++) {
                        if ((new File(Messages + "\\" + listOfUsers.get(i).getUsername() + ".txt")).exists()) {
                            Path incoming = Paths.get(Messages + "\\" + listOfUsers.get(i).getUsername() + ".txt");
                            try {
                                incomingMessages(incoming);
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }
                            k++;
                        }
                    }
                    if(k==0){
                        System.out.println("Messages's package is empty");
                    }
                }
                else if(w == 4){
                    profile();
                }
            }
            else {
                profile();
            }
        }
    }

    private void profile(){
        if(((Admin)admin).isAdmin()){
            System.out.println("0. Back");
        }
        System.out.println("1. Profile settings");
        System.out.println("2. Log out");
        System.out.println("3. Friends");
        System.out.println("4. Mail");
        int choice = sc.nextInt();
        if(choice==0){
            return;
        }
        if(choice==1){
            System.out.println(signedUser);
            System.out.println("1. Change name");
            System.out.println("2. Change surname");
            System.out.println("3. Change username");
            System.out.println("4. Change password");
            System.out.println("5. Back");
            System.out.println("6. Delete this account");
            int c = sc.nextInt();
            if (c == 1) {
                System.out.println(signedUser.getName());
                System.out.println("Write new name");
                String newName = null;
                try {
                    newName = reader.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                signedUser.setName(newName);
                System.out.println("Your name is changed");
            }
            if(c == 2){
                System.out.println(signedUser.getSurname());
                System.out.println("Write new surname");
                String newSurname = null;
                try {
                    newSurname = reader.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                signedUser.setSurname(newSurname);
                System.out.println("Your surname is changed");
            }
            if(c == 3){
                changeUsername();
            }
            if(c == 4){
                changePassword();
            }
            if (c == 5) {
            }
            if (c == 6){
                listOfUsers.remove(signedUser);
                System.out.println("Your account is deleted" + "\n");
                signedUser = null;
            }
        }
        else if(choice==2){
            signedUser = null;
        }
        else if(choice==3){
            signedUser.showFriendsList(signedUser);
            System.out.println("1. Add friend");
            System.out.println("2. Remove friend from your friend list");
            System.out.println("3. Show My friend list");
            System.out.println("4. Show all users registered");
            System.out.println("5. Back");
            int ch = sc.nextInt();
            if(ch==1){
                addFriendTolist();
            }
            else if(ch==2){
                removeFriendFromList();
            }
            else if(ch==3){
                signedUser.showFriendsList(signedUser);
            }
            else if (ch == 4){
                showUsers();
                System.out.println("\n");
            }
        }
        else if(choice==4){
            System.out.println("1. Incoming");
            System.out.println("2. Write message");
            System.out.println("3. Back");
            int k = sc.nextInt();
            if(k == 1) {
                if ((new File(Messages + "\\" + signedUser.getUsername() + ".txt")).exists()) {
                    System.out.println("All incoming messages" + "\n");
                    Path incoming = Paths.get(Messages + "\\" + signedUser.getUsername() + ".txt");
                    HashMap<String, String> receiver = new HashMap<>();
                    try {
                        receiver = incomingMessages(incoming);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    System.out.println("\n");
                    System.out.println("1. Answer");
                    System.out.println("2. Back");
                    int o = sc.nextInt();
                    if (o == 1) {
                        System.out.println(receiver);
                        k = 2;
                    } else if (o == 2) {
                    }
                }
                else{
                    System.out.println("You have no incoming messages");
                }
            }
            if(k == 2){
                System.out.println("Receiver's username");
                String message = null;
                try {
                    message = reader.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                sendMessage(message);
            }
            else if(k == 3){
            }
        }
    }


    private void sendMessage(String Uss){
        if(checkListUsername(Uss)) {
            File message = new File( Messages + "\\" + Uss + ".txt");
            System.out.println("Write your message");
            String text = null;
            try {
                text = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Message mail = new Message(signedUser.getUsername(),Uss,text);
            try {
                PrintWriter MessageWriter = new PrintWriter(new BufferedWriter(new FileWriter(message, true)));
                MessageWriter.println(mail);
                MessageWriter.flush();
                MessageWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Your message has been sent" + "\n");
        }
        else{
            System.out.println("There is no such user");
        }
    }

    private HashMap<String,String> incomingMessages(Path incoming) throws FileNotFoundException {
        Scanner reader = null;
        try {
            reader = new Scanner(incoming);
        } catch (IOException e) {
            e.printStackTrace();
        }
        HashMap<String, String> receiver = new HashMap<>();
        String resultStr = null;
        while(reader.hasNext()) {
            String msg = reader.nextLine();
            if (msg.contains("[") && msg.contains("|")) {
                resultStr = msg.substring(msg.indexOf('[') + 1, msg.indexOf('|'));
                String name = new String(), surname = new String();
                for(int i = 0 ;  i < listOfUsers.size(); i++){
                    if(resultStr.equals(listOfUsers.get(i).getUsername())){
                        name = listOfUsers.get(i).getName();
                        surname = listOfUsers.get(i).getSurname();
                    }
                }
                String NameSurname = name + " " + surname;
                receiver.put(resultStr,NameSurname);
            }
            System.out.println(msg);
        }
        return receiver;
    }

    private void changeUsername(){
        System.out.println(signedUser.getUsername());
        System.out.println("Write new username");
        String newUsername = null;
        try {
            newUsername = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(!checkListUsername(newUsername)) {
            signedUser.setUsername(newUsername);
            System.out.println("Your username is changed");
        }
        else{
            System.out.println("There exist such user");
            System.out.println("1. Try again");
            System.out.println("2. Back");
            int choice = sc.nextInt();
            if(choice==1) {
                changeUsername();
            }
            else if(choice==2){
                return;
            }
        }
    }

    private void changePassword() {
        System.out.println("Write your password");
        String password = null;
        try {
            password = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (password.equals(signedUser.getPassword().getPassword())) {
            System.out.println("Write your new password");
            String newPassword = null;
            try {
                newPassword = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Password paw = new Password();
            if(paw.setPassword(newPassword)) {
                signedUser.setPassword(paw);
                System.out.println("Your password is changed");
            }
            else{
                System.out.println("Incorrect Format of password");
                System.out.println("1. Try again");
                System.out.println("2. Back");
                int choice = sc.nextInt();
                if(choice==1) {
                    changePassword();
                }
                else if(choice==2){
                    return;
                }
            }
        }
        else{
            System.out.println("Incorrect password");
            System.out.println("1. Try again");
            System.out.println("2. Back");
            int choice = sc.nextInt();
            if(choice==1) {
                changePassword();
            }
            else if(choice==2){
                return;
            }
        }
    }



    private void addFriendTolist(){
        System.out.println("Write one username");
        String friendname = null;
        try {
            friendname = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        int count = 0;
        for(int m = 0; m < signedUser.getFriends().size(); m++){
            if(friendname.equals(signedUser.getFriends().get(m).getUsername())){
                System.out.println("You already have this friend in your friend list" + "\n");
                count++;
            }
        }
        if(count == 0) {
            for (int i = 0; i < listOfUsers.size(); i++) {
                User friend = listOfUsers.get(i);
                if (friendname.equals(friend.getUsername())) {
                    signedUser.addFriend(friend);
                    System.out.println("User is added to your friend list" + "\n");
                    return;
                }
            }
        }
        System.out.println("Incorrect friend's username");
        System.out.println("1. Try again");
        System.out.println("2. Back");
        int choice = sc.nextInt();
        if(choice==1) {
            addFriendTolist();
        }
        else{
            return;
        }
    }




    private void removeFriendFromList(){
        System.out.println("Write one username");
        String friendname = null;
        try {
            friendname = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < listOfUsers.size(); i++) {
            User friend = listOfUsers.get(i);
            if (friendname.equals(friend.getUsername())) {
                signedUser.removeFromFriend(friend);
                System.out.println("User is removed from your friend list");
                return;
            }
        }
        System.out.println("Incorrect friend's username");
        System.out.println("1. Try again");
        System.out.println("2. Back");
        int choice = sc.nextInt();
        if(choice==1) {
            removeFriendFromList();
        }
        else{
            return;
        }
    }




    private void userProfile() {
        signedUser = null;
    }




    private void authentication() {
        System.out.println("1. Sign in");
        System.out.println("2. Sign up");
        System.out.println("3. Back");
        int choice = sc.nextInt();
        if(choice==1){
            signIn();
        }
        else if(choice==2){
            signUp();
        }
    }




    private void signIn() {
        System.out.println("Enter your username");
        String u = null;
        try {
            u = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Enter your password");
        String p = null;
        try {
            p = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(checkListUsername(u) && checkListPassword(p)){
            System.out.println("Welcome!");
            for (int i = 0; i < listOfUsers.size(); i++) {
                User user = listOfUsers.get(i);
                if (u.equals(user.getUsername())) {
                    signedUser = user;
                    admin = new Admin(signedUser);
                }
            }
        }
        else{
            System.out.println("There is no such user. Or your account was banned.");
        }
    }




    private void signUp() {
        User curr = new User();
        System.out.println("Enter your name:");
        String n = null;
        try {
            n = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        curr.setName(n);
        System.out.println("Enter your surname:");
        String s = null;
        try {
            s = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        curr.setSurname(s);
        if(signUpUsername(curr) && signUpPassword(curr)) {
            listOfUsers.add(curr);
            signedUser = curr;
            System.out.println("Account is created" + "\n");
        }
        else return;
    }

    private boolean signUpUsername(User curr) {
        System.out.println("Enter your username:");
        String u = null;
        try {
            u = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (checkListUsername(u)) {
            System.out.println("There is exist such user");
            System.out.println("1. Try again");
            System.out.println("2. Back");
            int choice = sc.nextInt();
            if(choice==1) {
                signUpUsername(curr);
            }
            else{
                return false;
            }
        }
        else{
            curr.setUsername(u);
            return true;
        }
        return true;
    }




    private boolean signUpPassword(User curr){
        System.out.println("Enter your password (There should be uppercase, lowercase letters, numbers, more than 9 characters and password should't be the same as username)");
        String p = null;
        try {
            p = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Password pas = new Password();
        if(pas.setPassword(p) && !p.equals(curr.getUsername())) {
            curr.setPassword(pas);
            return true;
        }
        else{
            System.out.println("Incorrect Format of password");
            System.out.println("1. Try again");
            System.out.println("2. Back");
            int choice = sc.nextInt();
            if(choice==1) {
                signUpPassword(curr);
            }
            else{
                return false;
            }
        }
        return true;
    }





    private boolean checkListUsername(String username){
        if(!listOfUsers.isEmpty()) {
            for (int i = 0; i < listOfUsers.size(); i++) {
                User user = listOfUsers.get(i);
                if (username.equals(user.getUsername())) {
                    return true;
                }
            }
        }
        return false;
    }




    private boolean checkListPassword(String password){
        if(!listOfUsers.isEmpty()) {
            for (int i = 0; i < listOfUsers.size(); i++) {
                User user = listOfUsers.get(i);
                if (password.equals(user.getPassword().getPassword())) {
                    return true;
                }
            }
        }
        return false;
    }




    public void start() throws FileNotFoundException {
        System.out.println("Welcome to my application!");
        FillListWithUsers();
        while (true) {
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            }
            else if(choice == 2){
                try {
                    FileChannel.open(Paths.get(String.valueOf(file)), StandardOpenOption.WRITE).truncate(0).close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                saveUser();
                break;
            }
        }// save the userlist to db.txt
    }




    public void showUsers(){
        if(!listOfUsers.isEmpty() && signedUser!=null) {
            for (int i = 0; i < listOfUsers.size(); i++) {
                if(!signedUser.equals(listOfUsers.get(i))){
                    System.out.println(listOfUsers.get(i));
                }
            }
            if(listOfUsers.size()==1){
                System.out.println("There are no registered users");
            }
        }
    }




    private void saveUser() {
        try {
            for (int i = 0; i < listOfUsers.size(); i++) {
                String name = listOfUsers.get(i).getName();
                String surname = listOfUsers.get(i).getSurname();
                String userName = listOfUsers.get(i).getUsername();
                String password = listOfUsers.get(i).getPassword().getPassword();
                PrintWriter filewriter = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
                filewriter.println(name + " " + surname + " " + userName + " " + password);
                filewriter.flush();
                filewriter.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private void FillListWithUsers() throws FileNotFoundException {
        Scanner reader = new Scanner(file);
        while (reader.hasNext()) {
            String name = reader.next();
            String surname = reader.next();
            String username = reader.next();
            String password = reader.next();
            User user = new User();
            Password passwordObj = new Password();
            user.setName(name);
            user.setSurname(surname);
            user.setUsername(username);
            passwordObj.setPassword(password);
            user.setPassword(passwordObj);
            listOfUsers.add(user);
        }
        reader.close();
    }
}
