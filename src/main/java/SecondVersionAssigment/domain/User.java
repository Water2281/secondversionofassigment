package SecondVersionAssigment.domain;

import java.util.ArrayList;

public class User {
    private String name;
    private String surname;
    private String username;
    private Password password;
    private ArrayList<User> friends = new ArrayList<User>();

    public User(String name, String surname, String username, Password password) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
    }

    public User(){
    }


    public void setName(String name) {
        this.name = name;
    }

    public void addFriend(User friend){
        friends.add(friend);
    }

    public void removeFromFriend(User friend){
        friends.remove(friend);
    }

    public ArrayList<User> getFriends(){
        return friends;
    }

    public void showFriendsList(User u){
        if(!friends.isEmpty()) {
            for (int i = 0; i < friends.size(); i++) {
                if (!u.getUsername().equals(friends.get(i).getUsername())) {
                    System.out.println(friends.get(i));
                }
            }
        }
        else {
            System.out.println("You have no friends");
        }
    }

    public String getName() {
        return name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public Password getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return  name + " " + surname + " || Username: " + username;
    }
}


