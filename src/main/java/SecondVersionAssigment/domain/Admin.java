package SecondVersionAssigment.domain;

import java.util.ArrayList;

public class Admin extends User{
    private boolean isAdmin;

    public Admin(User admin) {
        super(admin.getName(),admin.getSurname(),admin.getUsername(),admin.getPassword());
        if(admin.getUsername().equals("dimeke") || admin.getUsername().equals("askar")){
            setAdmin(true);
        }
        else{
            setAdmin(false);
        }
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public Admin(){
    }

    @Override
    public String toString() {
        return super.toString() + " " + "Admin:" + isAdmin;
    }
}
