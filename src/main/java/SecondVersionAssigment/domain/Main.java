package SecondVersionAssigment.domain;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

public class Main {

    public static void main(String[] args) throws IOException {
        MyApplication application = new MyApplication();
        System.out.println("An application is about to start..");
        application.start();
    }
}
