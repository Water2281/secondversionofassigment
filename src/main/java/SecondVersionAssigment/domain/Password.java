package SecondVersionAssigment.domain;

public class Password {
    // passwordStr // it should contain uppercase and lowercase letters and digits
    // and its length must be more than 9 symbols
    private String password;

    public Password(){
    }

    public boolean setPassword(String password) {
        if(checkPassword(password)){
            this.password = password;
            return true;
        }
        else{
            return false;
        }
    }

    public String getPassword() {
        return password;
    }

    private boolean checkPassword(String password) {
        int counter1 = 0;
        int counter2 = 0;
        int counter3 = 0;
        if (password.length() >= 9) {
            for (int i = 0; i < password.length(); i++) {
                if (password.charAt(i) >= 65 && password.charAt(i) <= 90) {
                    counter1++;
                }
                if (password.charAt(i) >= 97 && password.charAt(i) <= 122) {
                    counter2++;
                }
                if (password.charAt(i) >= 48 && password.charAt(i) <= 57) {
                    counter3++;
                }
            }
            if(counter1>0 && counter2>0 && counter3>0){
                return true;
            }
            else{
                return false;
            }
        }
        return false;
    }
}
