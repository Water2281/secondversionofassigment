package SecondVersionAssigment.domain;

import java.util.Date;

public class Message {
    private String date;
    private String sender;
    private String receiver;
    private String text;

    public Message(String sender, String receiver, String text) {
        Date data = new Date();
        this.date = data.toString();
        setSender(sender);
        setReceiver(receiver);
        setText(text);
    }

    public String getDate() {
        return date;
    }

    public String getSender() {
        return sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public String getText() {
        return text;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "[" + sender + "|" + date + "]" + "\n" + text;
    }
}
